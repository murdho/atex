package rpn;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class RpnCalculatorTest {

    @Test
    public void newCalculatorHasZeroInItsAccumulator() throws Exception {
        RpnCalculator c = new RpnCalculator();

        assertThat(c.getAccumulator(), is(0));
    }

}

class RpnCalculator {

    public Integer getAccumulator() {
        return null;
    }
}