package gol;


import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class GameOfLifeTest {

    // Võiks alustada sellest, et kaadris on võimalik rakke elusaks märkida.

    // Siis võiks proovida naabreid lugeda

    // Siis võiks järgmist kaadrit arvutada.
    // Esialgu mõnda väga lihtsat.

    // Kui vajate abimeetodeid Nt. Frame.toString() siis kirjutage testid ka neile.

    // Kui kõik on valmis siis peaks testid stillWorksCorrectly(),
    // pulsarWorksCorrectly() ja gliderWorksCorrectly() töötama.
    // Soovitan esialgu neile pigem tähelepanu mitte pöörata ja teha asja sammhaaval.
    // Neid kontrollige alles siis, kui töö enamvähem valmis.

    @Test
    public void canMarkCellAlive() {
        Frame frame = new Frame(1, 1);

        frame.markAlive(1, 1);
        assertTrue(frame.isAlive(1, 1));
    }

    @Test
    public void canMarkCellDead() {
        Frame frame = new Frame(1, 1);

        frame.markDead(1, 1);
        assertTrue(!frame.isAlive(1, 1));
    }

    @Test
    public void cellAlivenessCanBeChecked() throws Exception {
        Frame frame = new Frame(1, 1);

        assertFalse(frame.isAlive(1, 1));
    }

    @Test
    public void hasStringRepresentation() throws Exception {
        String[] frameStr = {
                "------",
                "--XX--",
                "-X--X-",
                "--XX--",
                "------"};

        Frame frame = getFrame(frameStr);

        assertEquals(String.join("\n", frameStr), frame.toString());
    }

    @Test
    public void stillWorksCorrectly() {
        Frame frame = getFrame(
                "------",
                "--XX--",
                "-X--X-",
                "--XX--",
                "------");


        assertThat(frame.nextFrame(), is(equalTo(frame)));
    }

    @Test
    public void pulsarWorksCorrectly() {
        Frame frame = getFrame(
                "------",
                "-XX---",
                "-X----",
                "----X-",
                "---XX-",
                "------");

        Frame expected = getFrame(
                "------",
                "-XX---",
                "-XX---",
                "---XX-",
                "---XX-",
                "------");

        assertThat(frame.nextFrame(), is(equalTo(expected)));
        assertThat(frame.nextFrame().nextFrame(), is(equalTo(frame)));
    }

    @Test
    public void gliderWorksCorrectly() {
        Frame frame1 = getFrame(
                "-X----",
                "--XX--",
                "-XX---",
                "------");

        Frame frame2 = getFrame(
                "--X---",
                "---X--",
                "-XXX--",
                "------");

        Frame frame3 = getFrame(
                "------",
                "-X-X--",
                "--XX--",
                "--X---");

        assertThat(frame1.nextFrame(), is(equalTo(frame2)));
        assertThat(frame2.nextFrame(), is(equalTo(frame3)));
    }

    @Test
    public void neighboursCanBeCounted() throws Exception {
        Frame frame = getFrame(
                "-X----",
                "--XX--",
                "-XX---",
                "------");

        assertThat(frame.getNeighbourCount(2, 1), is(1));
        assertThat(frame.getNeighbourCount(3, 2), is(4));
    }

    private Frame getFrame(String... rows) {
        int width = rows[0].length();
        int height = rows.length;

        Frame frame = new Frame(width, height);

        for (int y = 1; y <= height; y++) {
            for (int x = 1; x <= width; x++) {
                char cellChar = rows[y - 1].charAt(x - 1);
                String cellStr = Character.toString(cellChar);

                if ("X".equals(cellStr)) {
                    frame.markAlive(x, y);
                }
            }
        }

        return frame;
    }
}
