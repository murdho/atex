package gol;

import java.util.Arrays;

public class Frame {

    public final static String ALIVE = "X";
    public final static String DEAD = "-";

    private final String[][] _matrix;
    private final int _height;
    private final int _width;

    /**
     * Create new Game of Life frame of specific width and height.
     * Frame is initialized with dead cells.
     *
     * @param width  width of the frame
     * @param height height of the frame
     */
    public Frame(int width, int height) {
        _height = height;
        _width = width;
        _matrix = new String[height][width];

        // Fill matrix with dead cells
        for (int y = 0; y < height; y++) {
            Arrays.fill(_matrix[y], DEAD);
        }
    }

    /**
     * Get string representation of the frame.
     * <p>
     * Example:
     * <p>
     * System.out.println(frame.toString());
     * ------
     * --XX--
     * -X--X-
     * --XX--
     * ------
     *
     * @return string representation of the frame
     */
    @Override
    public String toString() {
        String[] rows = new String[_height];

        for (int y = 0; y < _height; y++) {
            rows[y] = String.join("", _matrix[y]);
        }

        return String.join("\n", rows);
    }

    /**
     * Check the equality of this frame to provided object.
     * If provided object is not a Frame, equality is unattainable.
     * If provided object is a Frame, internal structures are compared.
     *
     * @param obj object to be compared with this frame
     * @return true if frames are equal, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Frame) {
            Frame otherFrame = (Frame) obj;
            return Arrays.deepEquals(_matrix, otherFrame._matrix);
        } else {
            return false;
        }
    }

    /**
     * Get the count of alive neighbours of a cell.
     *
     * @param x cell's X-coordinate
     * @param y cell's Y-coordinate
     * @return count of neighbours (min: 0; max: 8)
     */
    public Integer getNeighbourCount(int x, int y) {
        int neighbourCount = 0;

        int startX = Math.max(0, x - 2); // start lookup from left of cell
        int startY = Math.max(0, y - 2); // start lookup from above of cell
        int endX = Math.min(_width - 1, x); // end lookup at right of cell
        int endY = Math.min(_height - 1, y); // end lookup at below of cell

        for (int loopY = startY; loopY <= endY; loopY++) {
            for (int loopX = startX; loopX <= endX; loopX++) {
                // Don't count cell itself in the neighbours
                if (loopX == x - 1 && loopY == y - 1) continue;

                // Check if neighbouring cell is alive and well
                String cell = _matrix[loopY][loopX];
                if (ALIVE.equals(cell)) {
                    neighbourCount++;
                }
            }
        }

        return neighbourCount;
    }

    /**
     * Check, if cell with provided coordinates is alive.
     *
     * @param x cell's X-coordinate
     * @param y cell's Y-coordinate
     * @return true if cell is alive, false otherwise
     */
    public boolean isAlive(int x, int y) {

        String cell = _matrix[y - 1][x - 1];
        return ALIVE.equals(cell);
    }

    /**
     * Mark cell at provided coordinates to be alive.
     *
     * @param x cell's X-coordinate
     * @param y cell's Y-coordinate
     */
    public void markAlive(int x, int y) {
        _matrix[y - 1][x - 1] = ALIVE;
    }

    /**
     * Mark cell at provided coordinates to be dead.
     *
     * @param x cell's X-coordinate
     * @param y cell's Y-coordinate
     */
    public void markDead(int x, int y) {
        _matrix[y - 1][x - 1] = DEAD;
    }

    /**
     * Get the next frame. Takes into account neighbours of
     * every cell to have up to date values in the returned frame.
     *
     * @return new frame with updated state
     */
    public Frame nextFrame() {
        Frame newFrame = new Frame(_width, _height);

        for (int y = 1; y <= _height; y++) {
            for (int x = 1; x <= _width; x++) {
                int neighbourCount = getNeighbourCount(x, y);

                // only do something when cell's aliveness should change
                if (isAlive(x, y)) {
                    newFrame.markAlive(x, y);

                    if (neighbourCount < 2 || neighbourCount > 3) {
                        newFrame.markDead(x, y); // kill cell, it should be dead
                    }
                } else {
                    if (neighbourCount == 3) {
                        newFrame.markAlive(x, y); // resurrect cell, it should be alive
                    }
                }
            }
        }

        return newFrame;
    }

}
