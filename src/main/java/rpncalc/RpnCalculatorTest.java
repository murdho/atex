package rpncalc;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class RpnCalculatorTest {

    private RpnCalculator calc;

    @Before
    public void setUp() throws Exception {
        calc = new RpnCalculator();
    }

    @Test
    public void accumulatorIsZeroAtStart() {
        assertThat(calc.getAccumulator(), is(0));
    }

    @Test
    public void accumulatorCanBeSetToAValue() throws Exception {
        calc.setAccumulator(7);

        assertThat(calc.getAccumulator(), is(7));
    }

    @Test
    public void calculatorSupportsAddition() throws Exception {
        calc.setAccumulator(1);
        calc.enter();
        calc.setAccumulator(2);
        calc.plus();

        assertThat(calc.getAccumulator(), is(3));
    }

    @Test
    public void calculatorSupportsMultiplication() throws Exception {
        calc.setAccumulator(1);
        calc.enter();
        calc.setAccumulator(2);
        calc.plus();
        calc.enter();

        calc.setAccumulator(4);
        calc.multiply();

        assertThat(calc.getAccumulator(), is(12));
    }

    @Test
    public void calculatorSupportsMultipleParenthesis() throws Exception {
        calc.setAccumulator(4);
        calc.enter();
        calc.setAccumulator(3);
        calc.plus();
        calc.enter();

        calc.setAccumulator(1);
        calc.enter();
        calc.setAccumulator(2);
        calc.plus();

        calc.multiply();

        assertThat(calc.getAccumulator(), is(21));
    }

    @Test
    public void calculatorEvaluatesRPNString() throws Exception {
        assertThat(calc.evaluate("5 1 2 + 4 * + 3 +"), is(20));
    }
}
