package rpncalc;

import java.util.Stack;

public class RpnCalculator {
    private int accumulator;
    private Stack stack = new Stack();

    public int getAccumulator() {
        return accumulator;
    }

    public void setAccumulator(int i) {
        accumulator = i;
    }

    public void enter() {
        stack.push(accumulator);
    }

    public void plus() {
        accumulator += (int) stack.pop();
    }

    public void multiply() {
        accumulator *= (int) stack.pop();
    }

    public int evaluate(String rpnString) {
        for (Character currChar : rpnString.toCharArray()) {
            String currStr = currChar.toString();

            if (" ".equals(currStr)) {
                continue;
            } else if ("+".equals(currStr)) {
                plus();
            } else if ("*".equals(currStr)) {
                multiply();
            } else {
                enter();
                setAccumulator(Integer.parseInt(currStr));
            }
        }

        return accumulator;
    }
}
