package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ListPage extends AbstractPage {
    public ListPage(WebDriver driver) {
        super(driver);

        requireElementPresence("list_page", "not on list page");
    }

    public boolean userWithPasswordIsListed(String user, String pass) {
        boolean userListed = false;

        List<WebElement> rows = elementById("user_list")
                .findElements(By.tagName("div"));

        for (WebElement row : rows) {
            String username = row.getAttribute("username");
            String password = row.getAttribute("password");

            if (username.compareTo(user) == 0 && password.compareTo(pass) == 0) {
                userListed = true;
            }
        }

        return userListed;
    }
}
