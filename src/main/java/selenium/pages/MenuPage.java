package selenium.pages;

import org.openqa.selenium.WebDriver;

public class MenuPage extends AbstractPage {

    public MenuPage(WebDriver driver) {
        super(driver);

        requireElementPresence("menu_page", "not on menu page");
    }

    public LoginPage logOut() {
        clickElement("log_out_link");

        if (elementById("login_page") != null) {
            return new LoginPage(driver);
        }

        return null;
    }

    public ListPage showUsers() {
        clickElement("show_users_link");

        if (elementById("list_page") != null) {
            return new ListPage(driver);
        }

        return null;
    }

    public FormPage goToFormPage() {
        clickElement("add_user_link");

        if (elementById("form_page") != null) {
            return new FormPage(driver);
        }

        return null;
    }
}
