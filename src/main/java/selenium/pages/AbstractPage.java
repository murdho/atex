package selenium.pages;

import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public abstract class AbstractPage {

    public static final String BASE_URL = "http://enos.itcollege.ee/~mkalmo/selenium";

    protected final WebDriver driver;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
    }

    protected static WebDriver getDriver() {
        return new HtmlUnitDriver();
    }

    protected WebElement elementById(String id) {
        List<WebElement> elements = driver.findElements(By.id(id));
        return elements.isEmpty() ? null : elements.get(0);
    }

    protected void fillElement(String id, String contents) {
        elementById(id).sendKeys(contents);
    }

    protected void clickElement(String id) {
        elementById(id).click();
    }

    protected String getElementText(String id) {
        WebElement element = elementById(id);
        return element == null ? null : element.getText();
    }

    protected void requireElementPresence(String id, String failureMessage) {
        if (elementById(id) == null) {
            throw new IllegalStateException(failureMessage);
        }
    }
}
