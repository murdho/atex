package selenium.pages;

import org.openqa.selenium.WebDriver;

public class LoginPage extends AbstractPage {

    public LoginPage(WebDriver driver) {
        super(driver);

        requireElementPresence("login_page", "not on login page");
    }

    public MenuPage logIn(String user, String pass) {
        submitLoginForm(user, pass);

        if (getErrorMessage() == null) {
            return new MenuPage(driver);
        }

        return null;
    }

    public LoginPage logInWithExpectingFailure(String user, String pass) {
        submitLoginForm(user, pass);

        if (getErrorMessage() != null) {
            return this;
        }

        return null;
    }

    public LoginPage submitLoginForm(String user, String pass) {
        fillElement("username_box", user);
        fillElement("password_box", pass);
        clickElement("log_in_button");

        return this;
    }

    public String getErrorMessage() {
        return getElementText("error_message");
    }

    public static LoginPage goTo() {
        WebDriver driver = getDriver();
        driver.get(BASE_URL);
        return new LoginPage(driver);
    }
}
