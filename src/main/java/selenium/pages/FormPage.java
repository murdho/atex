package selenium.pages;

import org.openqa.selenium.WebDriver;

public class FormPage extends AbstractPage {
    public FormPage(WebDriver driver) {
        super(driver);

        requireElementPresence("form_page", "not on form page");
    }

    public MenuPage createNewUser(String user, String pass) {
        fillElement("username_box", user);
        fillElement("password_box", pass);
        clickElement("add_button");

        if (elementById("menu_page") != null) {
            return new MenuPage(driver);
        }

        return null;
    }
}
