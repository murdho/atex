package selenium;

import org.junit.Test;
import selenium.pages.FormPage;
import selenium.pages.ListPage;
import selenium.pages.LoginPage;
import selenium.pages.MenuPage;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class PageObjectTest {

    private static final String USERNAME = "user";
    private static final String CORRECT_PASSWORD = "1";
    private static final String WRONG_PASSWORD = "2";

    @Test
    public void loginFailsWithFalseCredentials() {
        LoginPage loginPage = LoginPage.goTo();

        loginPage.logInWithExpectingFailure(USERNAME, WRONG_PASSWORD);

        assertThat(loginPage.getErrorMessage(), is(notNullValue()));
    }

    @Test
    public void loginSucceedsWithCorrectCredentials() {
        MenuPage menuPage = goToMenuPage();

        assertThat(menuPage, is(notNullValue()));
    }

    @Test
    public void logoutIsPossible() throws Exception {
        LoginPage loginPage = goToMenuPage().logOut();

        assertThat(loginPage, is(notNullValue()));
    }

    @Test
    public void defaultUserIsListed() throws Exception {
        ListPage listPage = goToMenuPage().showUsers();

        assertTrue(listPage.userWithPasswordIsListed("user", "1"));
    }

    @Test
    public void newUserCreationIsPossible() throws Exception {
        FormPage formPage = goToMenuPage().goToFormPage();
        MenuPage menuPage = formPage.createNewUser("tom", "sawyer");
        ListPage listPage = menuPage.showUsers();

        assertTrue(listPage.userWithPasswordIsListed("tom", "sawyer"));
    }

    private MenuPage goToMenuPage() {
        return LoginPage.goTo().logIn(USERNAME, CORRECT_PASSWORD);
    }
}
