package custommock;

import common.BankService;
import common.Money;

import java.util.ArrayList;
import java.util.List;

public class TestableBankService implements BankService {

    List<String> callHistory = new ArrayList<>();
    private boolean fundsAvailable = true;

    public boolean wasCreditCalledWith(Money money, String account) {
        String callHistoryString = "credit:" + money + ":" + account;

        return callHistory.contains(callHistoryString);
    }

    public boolean wasDebitCalledWith(Money money, String account) {
        String callHistoryString = "debit:" + money + ":" + account;

        return callHistory.contains(callHistoryString);
    }

    @Override
    public void withdraw(Money money, String fromAccount) {
        System.out.println("credit: " + money + " - " + fromAccount);

        callHistory.add("credit:" + money + ":" + fromAccount);
    }

    @Override
    public void deposit(Money money, String toAccount) {
        System.out.println("debit: " + money + " - " + toAccount);

        callHistory.add("debit:" + money + ":" + toAccount);
    }

    @Override
    public Money convert(Money money, String targetCurrency) {
        if (money.getCurrency().equals(targetCurrency)) return money;

        double rate = 1.0/10;

        return new Money((int) (money.getAmount() / rate), targetCurrency);
    }

    @Override
    public String getAccountCurrency(String account) {
        switch (account) {
            case "E_123": return "EUR";
            case "S_456": return "SEK";
            default: throw new IllegalStateException();
        }
    }

    @Override
    public boolean hasSufficientFundsFor(Money requiredAmount, String account) {
        return fundsAvailable;
    }

    public void setSufficentFundsAvailable(boolean areFundsAvailable) {
        this.fundsAvailable = areFundsAvailable;
    }

    public boolean wasCreditCalled() {
        for (String record : callHistory) {
            if (record.startsWith("credit:")) {
                return true;
            }
        }

        return false;
    }

}