package invoice;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceRowGeneratorTest {

    @Mock
    InvoiceRowDao dao;

    @InjectMocks
    InoviceRowGenerator generator;

    InOrder inOrder;

    @Before
    public void setUp() throws Exception {
        inOrder = inOrder(dao);
    }

    @After
    public void tearDown() throws Exception {
        verifyNoMoreInteractions(dao);
    }

    @Test
    public void firstPaymentIsOnStartDate() throws Exception {
        generateRowsFor(1, "2016-12-15", "2017-03-03");

        verifyDaoSaveInvocations(1, getMatcherForDate("2016-12-15"));
    }

    @Test
    public void nextPaymentsAreOnFirstDayOfMonth() throws Exception {
        generateRowsFor(15, "2016-12-15", "2017-03-03");

        verifyDaoSaveInvocations(1, getMatcherForDate("2016-12-15"));
        verifyDaoSaveInvocations(1, getMatcherForDate("2017-01-01"));
        verifyDaoSaveInvocations(1, getMatcherForDate("2017-02-01"));
        verifyDaoSaveInvocations(1, getMatcherForDate("2017-03-01"));
    }

    @Test
    public void remainderGetsEvenlyDistributedStartingFromEnd() throws Exception {
        generateRowsFor(11, "2016-12-15", "2017-02-03");

        verifyDaoSaveInvocations(1, getMatcherForAmount(3));
        verifyDaoSaveInvocations(2, getMatcherForAmount(4));
    }

    @Test
    public void keepsPaymentAmountAtLeastThreeWhenPossible() throws Exception {
        generateRowsFor(7, "2016-12-15", "2017-03-03");

        verifyDaoSaveInvocations(1, getMatcherForAmount(3));
        verifyDaoSaveInvocations(1, getMatcherForAmount(4));
    }

    @Test
    public void handlesLargerThanMinimumButStillUndistributableAmount() throws Exception {
        generateRowsFor(5, "2016-12-15", "2017-03-03");

        verifyDaoSaveInvocations(1, getMatcherForAmount(5));
    }

    @Test
    public void onePaymentOnlyIfAmountLessThanThree() throws Exception {
        generateRowsFor(2, "2016-12-15", "2017-03-03");

        verifyDaoSaveInvocations(1, getMatcherForAmount(2));
    }

    @Test
    public void dividesEquallyWhenNoRemainder() {
        generateRowsFor(27, "2012-02-15", "2012-04-02");

        verifyDaoSaveInvocations(3, getMatcherForAmount(9));
    }

    @Test
    public void handlesBiggerAmountsAndLongerPeriodsAsWell() throws Exception {
        generateRowsFor(3000, "2016-12-22", "2021-12-22");

        verifyDaoSaveInvocations(50, getMatcherForAmount(49));
        verifyDaoSaveInvocations(11, getMatcherForAmount(50));
    }

    private Matcher<InvoiceRow> getMatcherForDate(final String dateString) {
        Date date = asDate(dateString);

        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return date.equals(item.date);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(dateString);
            }
        };
    }

    private Matcher<InvoiceRow> getMatcherForAmount(final Integer amount) {
        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return amount.equals(item.amount.intValue());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(String.valueOf(amount));
            }
        };
    }

    public static Date asDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private void generateRowsFor(int val, String start, String end) {
        generator.generateRowsFor(new BigDecimal(val), asDate(start), asDate(end));
    }

    private void verifyDaoSaveInvocations(int times, Matcher<InvoiceRow> matcher) {
        inOrder.verify(dao, times(times)).save(argThat(matcher));
    }

}