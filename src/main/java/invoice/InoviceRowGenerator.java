package invoice;

import org.joda.time.DateTime;
import org.joda.time.Months;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class InoviceRowGenerator {

    @SuppressWarnings("unused")
    private InvoiceRowDao invoiceRowDao;

    private static final BigDecimal MINIMUM_PAYMENT_AMOUNT = new BigDecimal(3);

    /**
     * Generate invoice rows for amount and period (defined by start date and end date)
     * The first payment will be on the start date, every following payment will be on the first day of the next month
     * until the month where end date is.
     *
     * @param amount amount to be divided into invoice rows
     * @param start  start date of the period; also payment date for first invoice row
     * @param end    end date of the period (period will end on the first day of the month this date falls into)
     */
    public void generateRowsFor(BigDecimal amount, Date start, Date end) {
        // Calculating maximum amount of payments between provided dates
        DateTime startDate = new DateTime(start).withDayOfMonth(1);
        DateTime endDate = new DateTime(end).withDayOfMonth(1);
        BigDecimal maxLength = new BigDecimal(Months.monthsBetween(startDate, endDate).getMonths() + 1);

        // Generating list of payment amounts
        List<BigDecimal> paymentAmounts = calculateAmounts(amount, MINIMUM_PAYMENT_AMOUNT, maxLength);

        // Creating invoice rows from calculated payment amounts
        for (int i = 0; i < paymentAmounts.size(); i++) {
            BigDecimal paymentAmount = paymentAmounts.get(i);
            Date paymentDate = nextPaymentDate(start, i);

            invoiceRowDao.save(new InvoiceRow(paymentAmount, paymentDate));
        }
    }

    /**
     * Calculates a list of payments as integer BigDecimals.
     * Caller can define the amount to be distributed,
     * minimum amount of one payment and maximum length of the period.
     *
     * @param totalAmount total amount to be divided into payments
     * @param minAmount   minimum amount of one payment (unless totalAmount is less than minAmount)
     * @param maxLength   maximum length of the period on which the payments should be divided
     * @return list of payment amounts with type BigDecimal (integers) where remainder is added evenly
     * starting from end of the period
     */
    private List<BigDecimal> calculateAmounts(BigDecimal totalAmount, BigDecimal minAmount, BigDecimal maxLength) {
        LinkedList<BigDecimal> paymentAmounts = new LinkedList<>();

        if (totalAmount.compareTo(minAmount) < 1) {
            // When total amount is less than minimum amount of one payment,
            // only one payment needs to be made
            paymentAmounts.add(totalAmount);
        } else {
            // Calculate, how many minimum values can be put to list
            BigDecimal[] numberOfMinValuesAndRemainder = totalAmount.divideAndRemainder(minAmount);
            BigDecimal numberOfMinValues = numberOfMinValuesAndRemainder[0];

            // How big amount is left over?
            BigDecimal remainder = numberOfMinValuesAndRemainder[1];

            // Calculate maximum length of the final list
            BigDecimal maxNumberOfMinValues = numberOfMinValues.min(maxLength);
            if (maxNumberOfMinValues.compareTo(numberOfMinValues) == -1) {
                // Calculate how much of amount will be in the min. values list
                BigDecimal sumOfAllowed = maxNumberOfMinValues.multiply(minAmount);

                // Calculate how much should be added to remainder
                // (the part that's not in the min. values list)
                BigDecimal sumNotAllowed = totalAmount.subtract(sumOfAllowed);

                // Add the calculated remainder to existing remainder
                remainder = remainder.add(sumNotAllowed);
            }

            // Add min. values to the list until limit is reached
            for (int i = 0; i < maxNumberOfMinValues.intValue(); i++) {
                paymentAmounts.add(minAmount);
            }

            // Perform remainder-related actions only when remainder is greater than zero
            if (remainder.compareTo(BigDecimal.ZERO) == 1) {
                // Calculate the length of list where remainder will be distributed
                // Has to be minimum of (1) remainder value, (2) max. length and (3) payment amounts list length
                BigDecimal paymentAmountsLength = new BigDecimal(paymentAmounts.size());
                BigDecimal additionalListLength = remainder.min(maxLength).min(paymentAmountsLength);

                // Create remainder distribution list with minimum value 1 (recursive)
                List<BigDecimal> additionalList = calculateAmounts(remainder, BigDecimal.ONE, additionalListLength);

                // Merge two lists by summing element-by-element (mutates paymentAmounts list)
                mergeSumTwoLists(paymentAmounts, additionalList);
            }
        }

        // Put payments into correct order
        Collections.sort(paymentAmounts);

        // Return final list
        return paymentAmounts;
    }

    /**
     * Merges together two lists of BigDecimals. Adds element from second list to the one in first list.
     * Mutates first (longer) list.
     *
     * @param longerList  longer list of BigDecimals (will be mutated)
     * @param shorterList shorter list of BigDecimals to add to longerList
     */
    private void mergeSumTwoLists(List<BigDecimal> longerList, List<BigDecimal> shorterList) {
        for (int i = 0; i < shorterList.size(); i++) {
            longerList.add(i,
                    longerList.remove(i).add(shorterList.get(i))
            );
        }
    }

    /**
     * Calculates next payment date using start date and offset (in months) from the date.
     *
     * @param start  any date from which the payment date should be calculated
     * @param offset number of months to add to the start date
     * @return date of first day of month which is offset distance from start date
     */
    private Date nextPaymentDate(Date start, int offset) {
        if (offset == 0) {
            return start;
        } else {
            return new DateTime(start).withDayOfMonth(1).plusMonths(offset).toDate();
        }
    }

}
