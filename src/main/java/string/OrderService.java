package string;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("unused")
public class OrderService {

    private DataSource dataSource;

    public OrderService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> getFilledOrders() {
        return orderStream().filter(Order::isFilled).collect(Collectors.toList());
    }

    public List<Order> getOrdersOver(double amount) {
        return orderStream().filter(o -> o.getTotal() > amount).collect(Collectors.toList());
    }

    public List<Order> getOrdersSortedByDate() {
        return orderStream().sorted(Comparator.comparing(Order::getOrderDate)).collect(Collectors.toList());
    }

    private Stream<Order> orderStream() {
        return dataSource.getOrders().stream();
    }
}
