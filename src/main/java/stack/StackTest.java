package stack;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@SuppressWarnings("unused")
public class StackTest {

    private Stack stack;

    @Before
    public void setUp() throws Exception {
        stack = new Stack(100);
    }

    @Test
    public void newStackHasNoElements() {
        assertThat(stack.getSize(), is(0));
    }

    @Test
    public void pushingIncreasesSize() {
        stack.push(3);
        stack.push(7);

        assertThat(stack.getSize(), is(2));
    }

    @Test
    public void poppingDecreasesSize() {
        stack.push(3);
        stack.push(7);

        stack.pop();
        stack.pop();

        assertThat(stack.getSize(), is(0));
    }

    @Test
    public void pushingAndPoppingReturnsOriginalElements() {
        stack.push(3);
        stack.push(7);

        assertThat(stack.pop(), is(7));
        assertThat(stack.pop(), is(3));
    }

    @Test
    public void peekShowsTopElement() {
        stack.push(3);
        stack.push(7);

        assertThat(stack.peek(), is(7));
    }

    @Test
    public void peekDoesNotRemoveElements() {
        stack.push(3);
        stack.push(7);

        stack.peek();

        assertThat(stack.getSize(), is(2));
    }

    @Test
    public void consecutivePeeksHaveSameResult() {
        stack.push(3);
        stack.push(7);

        int firstPeekRes = stack.peek();
        int secondPeekRes = stack.peek();

        assertThat(firstPeekRes, is(secondPeekRes));
    }

    @Test(expected = IllegalStateException.class)
    public void emptyStackPopThrows() {
        stack.pop();
    }

    @Test(expected = IllegalStateException.class)
    public void emptyStackPeekThrows() {
        stack.peek();
    }
}
