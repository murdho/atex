package stack;

public class Stack {
    private Integer[] elements;
    private int elemCount = 0;

    public Stack(int size) {
        elements = new Integer[size];
    }

    public int getSize() {
        return elemCount;
    }

    public void push(int element) {
        elements[elemCount += 1] = element;
    }

    public int pop() {
        int element = topElement();
        removeTopElement();

        return element;
    }

    public int peek() {
        return topElement();
    }

    private int topElement() {
        validateState();

        return elements[elemCount];
    }

    private void removeTopElement() {
        validateState();

        elements[elemCount] = null;
        elemCount -= 1;
    }

    private void validateState() {
        if (elemCount == 0)
            throw new IllegalStateException();
    }
}
