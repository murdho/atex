package stringcalc;

public class StringCalculator {
    public int add(String input) {

        // missing input causes exception
        if (null == input) throw new IllegalArgumentException();

        // empty string is 0
        if ("".equals(input)) return 0;

        // string with comma-separated numbers returns sum
        int sum = 0;
        for (String d : input.split(", ?")) {
            sum += Integer.parseInt(d);
        };

        return sum;
    }
}
