package stringcalc;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class StringCalculatorTest {

    static StringCalculator calculator;

    @Before
    public void setUp() throws Exception {
        calculator = new StringCalculator();
    }

    @Test
    public void emptyString_returnsZero() {
        assertThat(
                calculator.add(""),
                is(0)
        );
    }

    @Test
    public void singleNumber_returnsValue() throws Exception {
        assertThat(
                calculator.add("1"),
                is(1)
        );
    }

    @Test
    public void multipleNumbers_returnsSum() throws Exception {
        assertThat(
                calculator.add("1, 2, 3"),
                is(6)
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void missingInput_throws() throws Exception {
        calculator.add(null);
    }
}
