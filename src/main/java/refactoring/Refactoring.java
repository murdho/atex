package refactoring;

import java.util.*;

public class Refactoring {

    // 1a
    public int getNextInvoiceNumber() {
        return ++invoiceNumber;
    }

    // 1b
    public void addFilledOrdersToList(List<Order> orderList) {
        for (Order order : orders) {
            if (order.isFilled()) {
                orderList.add(order);
            }
        }
    }

    // 2a
    public void printStatementFor(Long invoiceId) {
        List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);

        printInvoiceRows(invoiceRows);

        // calculate invoice total
        double total = calcInvoiceTotal(invoiceRows);

        printValue(total);
    }

    public double calcInvoiceTotal(List<InvoiceRow> invoiceRows) {
        double total = 0;
        for (InvoiceRow invoiceRow : invoiceRows) {
            total += invoiceRow.getAmount();
        }
        return total;
    }

    // 2b
    public String getItemsAsHtml() {
        String listItems = "";

        List<String> items = Arrays.asList(item1, item2, item3, item4);
        for (String item : items) {
            listItems += htmlTag("li", item);
        }

        return htmlTag("ul", listItems);
    }

    public String htmlTag(String name, String content) {
        return "<" + name + ">" + content + "</" + name + ">";
    }

    // 3
    public boolean isSmallOrder() {
        return (order.getTotal() > 100);
    }

    // 4
    public void printPrice() {
        System.out.println("Hind ilma käibemaksuta: " + getBasePrice());
        System.out.println("Hind käibemaksuga: " + getBasePriceGross());
    }

    // 5
    public void calculatePayFor(Job job) {
        // on holiday at night
        boolean jobOnWeekend = job.day == 6 || job.day == 7;
        boolean jobAtNight = job.hour > 20 || job.hour < 7;

        if (jobOnWeekend && jobAtNight)  {
            // calculate $$$
        }
    }

    // 6
    public boolean canAccessResource(SessionData sessionData) {
        // is admin and has preferred status

        return userIsAdmin(sessionData) && statusIsPreferred(sessionData);
    }

    public boolean statusIsPreferred(SessionData sessionData) {
        String status = sessionData.getStatus();

        return status.equals("preferredStatusX") || status.equals("preferredStatusY");
    }

    public boolean userIsAdmin(SessionData sessionData) {
        String currentUserName = sessionData.getCurrentUserName();

        return currentUserName.equals("Admin") || currentUserName.equals("Administrator");
    }

    // 7
    public void drawLines() {
        Space space = new Space();
        space.drawLine(new Point(12, 3, 5), new Point(2, 4, 6));
        space.drawLine(new Point(2, 4, 6), new Point(0, 1, 0));
    }

    // 8
    public int calculateWeeklyPayWithOvertime(int hoursWorked) {
        int straightTime = Math.min(40, hoursWorked);
        int straightPay = straightTime * hourRate;

        int overTime = Math.max(0, hoursWorked - straightTime);
        double overtimeRate = 1.5 * hourRate;
        int overtimePay = (int) Math.round(overTime * overtimeRate);

        return straightPay + overtimePay;
    }

    public int calculateWeeklyPayWithoutOvertime(int hoursWorked) {
        return hoursWorked * hourRate;
    }

    // //////////////////////////////////////////////////////////////////////////

    // Abiväljad ja abimeetodid.
    // Need on siin lihtsalt selleks, et kood kompileeruks

    public static final double GROSS_MULTIPLIER = 1.2;

    private String item1 = "1";
    private String item2 = "2";
    private String item3 = "3";
    private String item4 = "4";
    private int hourRate = 5;
    int invoiceNumber = 0;
    private List<Order> orders = new ArrayList<Order>();
    private Order order = new Order();
    private Dao dao = new SampleDao();
    private double price = 0;

    void justACaller() {
        getNextInvoiceNumber();
        addFilledOrdersToList(null);
    }

    private void printValue(double total) {
    }

    private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
    }

    class Space {
        public void drawLine(Point startPoint, Point endPoint) {
        }

    }

    interface Dao {
        List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
    }

    class SampleDao implements Dao {
        @Override
        public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
            return null;
        }
    }

    class Order {
        public boolean isFilled() {
            return false;
        }

        public double getTotal() {
            return 0;
        }
    }

    class InvoiceRow {
        public double getAmount() {
            return 0;
        }
    }

    class Job {
        public int hour;
        public int day;
    }

    private double getBasePrice() {
        return price;
    }

    private double getBasePriceGross() {
        return getBasePrice() * GROSS_MULTIPLIER;
    }

    private class SessionData {

        public String getCurrentUserName() {
            return null;
        }

        public String getStatus() {
            return null;
        }

    }

    private class Point {
        private final int x;
        private final int y;
        private final int z;

        public Point(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
}
