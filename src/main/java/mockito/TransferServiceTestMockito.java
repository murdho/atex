package mockito;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.mockito.Mockito;

import common.BankService;
import common.TransferService;
import common.Money;

@SuppressWarnings("unused")
public class TransferServiceTestMockito {

    BankService bankService = mock(BankService.class);
    TransferService transferService = new TransferService(bankService);

    @Test
    public void transferWithCurrencyConversion() {
        when(bankService.getAccountCurrency("E_123")).thenReturn("EUR");
        when(bankService.getAccountCurrency("S_456")).thenReturn("SEK");

        when(bankService.convert(anyMoney(), eq("EUR"))).thenReturn(new Money(1, "EUR"));
        when(bankService.convert(anyMoney(), eq("SEK"))).thenReturn(new Money(10, "SEK"));

        when(bankService.hasSufficientFundsFor(anyMoney(), anyAccount())).thenReturn(true);

        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");

        verify(bankService).withdraw(new Money(1, "EUR"), "E_123");
        verify(bankService).deposit(new Money(10, "SEK"), "S_456");
    }

    @Test
    public void transferWhenNotEnoughFunds() {
        when(bankService.hasSufficientFundsFor(anyMoney(), anyAccount())).thenReturn(false);

        transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");

        verify(bankService, never()).withdraw(anyMoney(), anyAccount());
        verify(bankService, never()).deposit(anyMoney(), anyAccount());
    }

    private Money anyMoney() {
        return (Money) any();
    }

    private String anyAccount() {
        return anyString();
    }
}