package sql;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SelectBuilder {

    private List<String> _columns;
    private List<String> _tables;
    private List<String> _joins;
    private List<String> _conditions;
    private List<Object> _parameters;

    public SelectBuilder() {
        _columns    = new ArrayList<String>();
        _tables     = new ArrayList<String>();
        _joins      = new ArrayList<String>();
        _conditions = new ArrayList<String>();
        _parameters = new ArrayList<Object>();
    }

    public String getSql() {
        StringBuilder sql = new StringBuilder();

        // SELECT
        sql.append("select ");
        sql.append(String.join(", ", _columns));

        // FROM
        if (!_tables.isEmpty()) {
            sql.append(" from ");
            sql.append(String.join(", ", _tables));
        }

        // JOINS
        if (!_joins.isEmpty()) {
            sql.append(" left join ");
            sql.append(String.join(" and ", _joins));
        }

        // WHERE
        if (!_conditions.isEmpty()) {
            sql.append(" where ");
            sql.append(String.join(" and ", _conditions));
        }

        return sql.toString();
    }

    public List<Object> getParameters() {
        return _parameters;
    }

    public void column(String column) {
        _columns.add(column);
    }

    public void columns(String ... columns) {
        Collections.addAll(_columns, columns);
    }

    public void from(String table) {
        _tables.add(table);
    }

    public void from(SelectBuilder sub) {
        _tables.add("(" + sub.getSql() + ")");
    }

    public void where(String condition, Object parameter) {
        _conditions.add(condition);
        _parameters.add(parameter);
    }

    public void where(String condition) {
        _conditions.add(condition);
    }

    public void eqIfNotNull(String column, Object parameter) {
        // Return if parameter is missing
        if (parameter == null) return;

        _conditions.add(column + " = ?");
        _parameters.add(parameter);
    }

    public void leftJoin(String table, String condition) {
        _joins.add(table + " on " + condition);
    }

    public void in(String column, List<Object> parameters) {
        // Return if no parameters provided
        if (parameters.isEmpty()) return;

        // Add all parameters to internal collection
        Collections.addAll(_parameters, parameters.toArray());

        // Start building condition
        StringBuilder condition = new StringBuilder();

        condition.append(column);
        condition.append(" in (");
        for (int i = 0; i < parameters.size() - 1; i++) {
            condition.append("?, ");
        }
        condition.append("?)");

        // Add condition to internal collection
        _conditions.add(condition.toString());
    }
}
