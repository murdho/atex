package sql;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SelectBuilderTest {

    private SelectBuilder builder;

    @Before
    public void setUp() {
        builder = new SelectBuilder();
    }

    @Test
    public void singleColumnSelectWithoutTable() {
        builder.column("now()");

        assertThat(builder.getSql(), is("select now()"));
    }

    @Test
    public void singleColumnSelectFromTable() {
        builder.column("a");
        builder.from("t");

        assertThat(builder.getSql(), is("select a from t"));
    }

    @Test
    public void multiColumnSelectFromTable() {
        builder.columns("a", "b");
        builder.from("t");

        assertThat(builder.getSql(), is("select a, b from t"));
    }

    @Test
    public void singleWhereConditionWithParameter() {
        builder.column("a");
        builder.from("t");
        builder.where("id = ?", 1);

        assertThat(builder.getSql(), is("select a from t where id = ?"));
        assertThat(builder.getParameters(), is(Arrays.asList(1)));

    }

    @Test
    public void multipleWhereConditionsWithoutParameters() {
        builder.column("a");
        builder.from("t");
        builder.where("is_hidden = 1");
        builder.where("deleted_on is null");

        assertThat(builder.getSql(),
                is("select a from t where is_hidden = 1 and deleted_on is null"));
    }

    @Test
    public void whereConditionsWithParametersIfNotNull() {
        builder.column("a");
        builder.from("t");
        builder.eqIfNotNull("a", 1);
        builder.eqIfNotNull("b", null);
        builder.eqIfNotNull("c", 3);

        assertThat(builder.getSql(), is("select a from t where a = ? and c = ?"));
        assertThat(builder.getParameters(), is(Arrays.asList(1, 3)));
    }

    @Test
    public void whereConditionWithInClauseAndWithParameters() {
        builder.column("a");
        builder.from("t");
        builder.in("id", Arrays.asList(1, 2));

        assertThat(builder.getSql(), is("select a from t where id in (?, ?)"));
        assertThat(builder.getParameters(), is(Arrays.asList(1, 2)));
    }

    @Test
    public void whereConditionWithInClauseAndWithoutParameters() {
        builder.column("a");
        builder.from("t");
        builder.in("id", Collections.emptyList());

        assertThat(builder.getSql(), is("select a from t"));
    }

    @Test
    public void leftJoinWithJoinCondition() {
        builder.column("a");
        builder.from("t");
        builder.leftJoin("u", "u.id = t.u_id");

        assertThat(builder.getSql(), is("select a from t left join u on u.id = t.u_id"));
    }

    @Test
    public void selectFromAnotherSelectBuilderAsSubquery() {
        SelectBuilder subBuilder = new SelectBuilder();
        subBuilder.column("b");
        subBuilder.from("t");


        builder.column("a");
        builder.from(subBuilder);

        assertThat(builder.getSql(), is("select a from (select b from t)"));
    }
}

